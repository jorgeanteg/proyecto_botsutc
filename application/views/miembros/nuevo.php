<style>
    form {
        background-color: #ffffff;
        box-shadow: 0 0 20px rgb(0, 0, 0, 0.20);
        padding: 20px 30px;
    }
</style>
<div class="container">
    <H1 class="text-center">NUEVO MIEMBRO</H1>
    <br>
    <form class="" action="<?php echo site_url(); ?>/miembros/guardar" method="post">
        <div class="row">
            <div class="col-md-6">
                <label for="">Cédula:</label>
                <br>
                <input type="number" placeholder="Ingrese la cédula" class="form-control" name="cedula_est" value=""
                    id="cedula_est">
            </div>
            <div class="col-md-6">
                <label for="">Apellidos:</label>
                <br>
                <input type="text" placeholder="Ingrese sus apellidos" class="form-control"
                    name="apellidos_est" value="" id="apellidos_est">
            </div>
        </div>
        <br>
        <br>

        <div class="row">
            <div class="col-md-6">
                <label for="">Nombres:</label>
                <br>
                <input type="text" placeholder="Ingrese sus nombres" class="form-control"
                    name="nombres_est" value="" id="nombres_est">
            </div>
            <div class="col-md-6">
                <label for="">Dirección:</label>
                <br>
                <input type="text" placeholder="Ingrese su dirección" class="form-control" name="direccion_est" value=""
                    id="direccion_est">
            </div>
        </div>
        <br>
    <br>
        <div class="row">
            <div class="col-md-6">
                <label for="">Carrera:</label>
                <br>
                <input type="text" placeholder="Ingrese su carrera" class="form-control" name="carrera_est" value=""
                    id="carrera_est">
            </div>
            <div class="col-md-6">
                <label for="">Ciclo:</label>
                <br>
                <input type="text" placeholder="Ingrese el ciclo" class="form-control" name="ciclo_est" value=""
                    id="ciclo_est">
            </div>
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/miembros/listado" class="btn btn-danger">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
    <br>
        <br>
        <br>

</div>