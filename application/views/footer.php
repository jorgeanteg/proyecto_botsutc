<style>
    footer {
        background-color: #024059;
        color:white;
    }
</style>

<footer>
    <div class="container">
        <div class="row">
            <br>
            <div class="col-md-6 text-center">
                <h5>DESARROLLADO POR:</h5>
                <p>Jorge Ante</p>
                <p>Información Legal</p>
                <p>Politicas de privacidad</p>
            </div>
            <div class="col-md-6 text-center">
                <h5>LOCALIZACIÓN:</h5>
                <p>Ecuador</p>
                <p>Cotopaxi</p>
                <p>Latacunga</p>
            </div>
        </div>
        <hr>
        <p class="text-center">BOTS UTC Derechos reservados</p>
        <br>
    </div>
</footer>



</body>

</html>