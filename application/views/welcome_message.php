<style>
    .inicio {
        background-color: #E4F2F1;
    }
    .titulo {
        font-size: 40px;
    }

    .inicio h2 {
        font-size: 50px;
    }

    .img-portada {
        filter: drop-shadow(-8px 20px 20px black);
    }

    .btn {
        padding: 10px 30px;
    }

    
</style>


<section class="inicio">
    <div class="container">
        <div class="text-center">
            <h1 class="titulo">CLUB DE ROBÓTICA BOT'S UTC</h1>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>BIENVENIDOS</h2>
                <br>
                <p>Boot´s UTC busca ser un conjunto de alumnos de diferentes carreras aportando la creación e innovando
                    de
                    las nuevas tecnologías, logrando obtener nuevos profesionales con destrezas que le asegure un éxito
                    futuro, para con ello dar un aporte investigativo y de bajo costo a la sociedad, mejorando la
                    calidad de vida de las personas</p>
                <br>
                <p>Ven y forma parte de nuestro CLUB</p>
                <br>
                <a class="btn btn-primary" href="<?php echo site_url(); ?>/ubicanos/ubicanos">Ubícanos</a>
                <br>
            </div>
            <div class="col-md-6 text-center">
                <img class="img-portada" src="<?php echo base_url(); ?>/assets/img/portada.png" alt="imagen de un robot bailarin">
            </div>
        </div>
    </div>
</section>