<style>
    form {
        background-color: #ffffff;
        box-shadow: 0 0 20px rgb(0, 0, 0, 0.20);
        padding: 20px 30px;
    }
</style>
<div class="container">
    <H1 class="text-center">NUEVO EVENTO</H1>
    <br>
    <form class="" action="<?php echo site_url(); ?>/eventos/guardar" method="post">
        <div class="row">
            <div class="col-md-6">
                <label for="">Nombre del Evento:</label>
                <br>
                <input type="text" placeholder="Ingrese el nombre" class="form-control" name="nombre_eve" value=""
                    id="nombre_eve">
            </div>
            <div class="col-md-6">
                <label for="">Descripción:</label>
                <br>
                <input type="text" placeholder="Ingrese su descripcion" class="form-control" name="descripcion_eve"
                    value="" id="descripcion_eve">
            </div>
        </div>
        <br>
        <br>

        <div class="row">
            <div class="col-md-6">
                <label for="">Fecha Inicio:</label>
                <br>
                <input type="date" placeholder="Ingrese la fecha de inicio" class="form-control" name="fecha_inicio_eve"
                    value="" id="fecha_inicio_eve">
            </div>
            <div class="col-md-6">
                <label for="">Fecha Final:</label>
                <br>
                <input type="date" placeholder="Ingrese la fecha de finalizacion" class="form-control"
                    name="fecha_final_eve" value="" id="fecha_final_eve">
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label for="">Responsable:</label>
                <br>
                <input type="text" placeholder="Ingrese el Responsable" class="form-control" name="responsable_eve"
                    value="" id="responsable_eve">
            </div>
            <div class="col-md-6">
                <label for="">Lugar:</label>
                <br>
                <input type="text" placeholder="Ingrese el lugar" class="form-control" name="lugar_eve" value=""
                    id="lugar_eve">
            </div>
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/eventos/listado" class="btn btn-danger">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
    <br>
    <br>
    <br>

</div>