<style>
    .nosotros {
        background-color: #EAF2F1;
    }
    .carousel-inner {
        width: 100%;
        height: 500px;
    }

    .carousel {
        margin: 0 10%;
    }

    .img-logo-nosotros {
        width: 200px;
    }
</style>




<section class="nosotros">
    <div class="container">
        <div>
            <h1 class="text-center">NOSOTROS</h1>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?php echo base_url(); ?>/assets/img/slider1.jpg" alt="slider 1">
                        </div>

                        <div class="item">
                            <img src="<?php echo base_url(); ?>/assets/img/slider2.jpg" alt="slider 2">
                        </div>

                        <div class="item">
                            <img src="<?php echo base_url(); ?>/assets/img/slider4.jpg" alt="slider 3">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <br>



        <div class="row">
            <div class="text-center">
                <img class="img-logo-nosotros" src="<?php echo base_url(); ?>/assets/img/LogoClub (2).png" alt="Logo del club">
            </div>
            <br>
            <div class="text-center">
                <h3><b>SOBRE NOSOTROS</b></h3>
            </div>
            <div class="text-center">
                <p>Somos un grupo de estudiantes interesados en el manejo e implementación de la robótica, con la
                    finalidad de difundir e intercambiar conocimientos entre estudiantes de todas las carreras que
                    conforman nuestra universidad.</p>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-6 text-center">
                <h3><b>MISIÓN</b></h3>
                <p>Desarrollar e innovar la tecnología en la robótica, fortaleciendo los conocimientos de los estudiantes de nuestra querida alma mater.</p>
            </div>
            <div class="col-md-6 text-center">
                <h3><b>VISIÓN</b></h3>
                <p>Boot´s UTC busca ser un conjunto de alumnos de diferentes carreras aportando la creación e innovando de las nuevas tecnologías, logrando obtener nuevos profesionales con destrezas que le asegure un éxito futuro, para con ello dar un aporte investigativo y de bajo costo a la sociedad, mejorando la calidad de vida de las personas.</p>
            </div>
        </div>
        <br>
    </div>






</section>