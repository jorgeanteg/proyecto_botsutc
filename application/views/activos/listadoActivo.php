<style>
    .contenedor {
        background-color: #E4F2F1;
        color: rgb(0, 0, 0);
    }

    .table {
        background-color: #ffffff;
        box-shadow: 0 0 20px rgb(0, 0, 0, 0.20);
    }

    .table thead tr {
        background-color: #418fbf;
        color: #ffffff;
    }

    .table tbody tr:last-of-type {
        border-bottom: 2px solid #418FBF;
    }

    .table tbody tr:hover {
        background-color: #908E8F;
        color: #ffffff;
    }
    .bton-nuevo {
        margin-top: 18px;
    }
</style>

<section class="contenedor">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 class="text-center">Listado de Activos del CLUB</h1>
            </div>
            <div class="col-md-4 bton-nuevo">
                <a href="<?php echo site_url(); ?>/activos/nuevoActivo" class="btn btn-primary">
                    <i class="glyphicon glyphicon-plus"> </i>
                    Agregar Activo
                </a>
            </div>
        </div>

        <br>
        <?php if ($activos): ?>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>ESTADO</th>
                        <th>FECHA DE INGRESO</th>
                        <th>UBICACIÓN</th>
                        <th>CANTIDAD</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($activos as $filaTemporal): ?>
                        <tr>
                            <td>
                                <?php echo $filaTemporal->id_act; ?>
                            </td>
                            <td>
                                <?php echo $filaTemporal->nombre_act; ?>
                            </td>
                            <td>
                                <?php echo $filaTemporal->estado_act; ?>
                            </td>
                            <td>
                                <?php echo $filaTemporal->fecha_ingreso_act; ?>
                            </td>
                            <td>
                                <?php echo $filaTemporal->ubicacion_act; ?>
                            </td>
                            <td>
                                <?php echo $filaTemporal->cantidad_act; ?>
                            </td>


                            <td class="text-center">
                                <a href="#" title="Editar Activo">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url(); ?>/activos/eliminar/<?php echo $filaTemporal->id_act; ?>"
                                    title="Eliminar Activo" onclick="return confirm('¿Está seguro de eliminar este registro?');"
                                    style="color:red;">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>

        <?php else: ?>
            <h1>No hay Activos</h1>
        <?php endif; ?>

    </div>

</section>