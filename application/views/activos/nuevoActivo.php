<style>
    form {
        background-color: #ffffff;
        box-shadow: 0 0 20px rgb(0, 0, 0, 0.20);
        padding: 20px 30px;
    }
</style>
<div class="container">
    <H1 class="text-center">NUEVO ACTIVO</H1>
    <br>
    <form class="" action="<?php echo site_url(); ?>/activos/guardar" method="post">
        <div class="row">
            <div class="col-md-6">
                <label for="">Nombre:</label>
                <br>
                <input type="text" placeholder="Ingrese el nombre" class="form-control" name="nombre_act" value=""
                    id="nombre_act">
            </div>
            <div class="col-md-6">
                <label for="">Estado:</label>
                <br>
                <input type="text" placeholder="Ingrese el estado" class="form-control"
                    name="estado_act" value="" id="estado_act">
            </div>
        </div>
        <br>
        <br>

        <div class="row">
            <div class="col-md-6">
                <label for="">Fecha de ingreso:</label>
                <br>
                <input type="date" placeholder="Ingrese la fecha" class="form-control"
                    name="fecha_ingreso_act" value="" id="fecha_ingreso_act">
            </div>
            <div class="col-md-6">
                <label for="">Ubicación:</label>
                <br>
                <input type="text" placeholder="Ingrese su dirección" class="form-control" name="ubicacion_act" value=""
                    id="ubicacion_act">
            </div>
        </div>
        <br>
    <br>
        <div class="row">
            <div class="col-md-6">
                <label for="">Cantidad:</label>
                <br>
                <input type="text" placeholder="Ingrese la cantidad" class="form-control" name="cantidad_act" value=""
                    id="cantidad_act">
            </div>
            
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/activos/listadoActivo" class="btn btn-danger">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
    <br>
        <br>
        <br>

</div>