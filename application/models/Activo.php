<?php 

    class Activo extends CI_Model 
    {
        function __construct() 
        {
            parent::__construct();
        }

        function insertar($datos){
            //ACTIVE RECORD -> CODEiGNITER
            return $this->db->insert("activos",$datos);
        }

        function obtenerTodos(){
            $listadoActivos=$this->db->get("activos");//esto devuelve un array
            if($listadoActivos->num_rows()>0) { //si hay datos
                return $listadoActivos->result();
            }else{ //si no hay datos
                return false;
            }
        }

        function borrar($id_act){
            //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
            $this->db->where("id_act",$id_act);

            //instructor tabla de base de datos
            if ($this->db->delete("activos")) {
                return true;
            } else {
                return false;
            }
            
            //El codigo de arriba en una solo linea
            // return $this->db->delete("instructor");
        }

    }




?>