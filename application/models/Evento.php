<?php 

    class Evento extends CI_Model 
    {
        function __construct() 
        {
            parent::__construct();
        }

        function insertar($datos){
            //ACTIVE RECORD -> CODEiGNITER
            return $this->db->insert("eventos",$datos);
        }

        function obtenerTodos(){
            $listadoEventos=$this->db->get("eventos");//esto devuelve un array
            if($listadoEventos->num_rows()>0) { //si hay datos
                return $listadoEventos->result();
            }else{ //si no hay datos
                return false;
            }
        }

        function borrar($id_est){
            //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
            $this->db->where("id_eve",$id_est);

            //instructor tabla de base de datos
            if ($this->db->delete("eventos")) {
                return true;
            } else {
                return false;
            }
            
            //El codigo de arriba en una solo linea
            // return $this->db->delete("instructor");
        }

    }




?>