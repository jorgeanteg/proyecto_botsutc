<?php

class Miembros extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Miembro');
    }
    function listado()
    {
        $data['miembros']=$this->Miembro->obtenerTodos();
        $this->load->view('header');
        $this->load->view('miembros/listado', $data);
        $this->load->view('footer');
    }

    function nuevo()
    {
        $this->load->view('header');
        $this->load->view('miembros/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoEstudiante = array(
            "cedula_est" => $this->input->post('cedula_est'),
            "apellidos_est" => $this->input->post('apellidos_est'),
            "nombres_est" => $this->input->post('nombres_est'),
            "direccion_est" => $this->input->post('direccion_est'),
            "carrera_est" => $this->input->post('carrera_est'),
            "ciclo_est" => $this->input->post('ciclo_est'),
        );

        if($this->Miembro->insertar($datosNuevoEstudiante)){
            redirect('miembros/listado');
        }else{
            echo "<h1> Error al insertar</h1>";
        }

    }

    public function eliminar($id_est){
        if ($this->Miembro->borrar($id_est)) {
            redirect('miembros/listado');
            # code...
        } else {
            # code...
            echo "ERROR AL BORRAR :(";
        }
        
    }


    
}





?>