<?php

class Activos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Activo');
    }
    function listadoActivo()
    {
        $data['activos']=$this->Activo->obtenerTodos();
        $this->load->view('header');
        $this->load->view('activos/listadoActivo', $data);
        $this->load->view('footer');
    }

    function nuevoActivo()
    {
        $this->load->view('header');
        $this->load->view('activos/nuevoActivo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoActivos = array(
            "nombre_act" => $this->input->post('nombre_act'),
            "estado_act" => $this->input->post('estado_act'),
            "fecha_ingreso_act" => $this->input->post('fecha_ingreso_act'),
            "ubicacion_act" => $this->input->post('ubicacion_act'),
            "cantidad_act" => $this->input->post('cantidad_act'),
            
        );

        if($this->Activo->insertar($datosNuevoActivos)){
            redirect('activos/listadoActivo');
        }else{
            echo "<h1> Error al insertar</h1>";
        }

    }

    public function eliminar($id_act){
        if ($this->Activo->borrar($id_act)) {
            redirect('activos/listadoActivo');
            # code...
        } else {
            # code...
            echo "ERROR AL BORRAR :(";
        }
        
    }


    
}





?>