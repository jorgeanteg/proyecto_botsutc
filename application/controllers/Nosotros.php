<?php
    class Nosotros extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
        }

        //Funcion que renderiza la pagina nosotros

        public function nosotros() {
            $this->load->view('header');
            $this->load->view('nosotros/nosotros');
            $this->load->view('footer');
        }

    }

?>