<?php

class Eventos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Evento');
    }
    function listado()
    {
        $data['eventos']=$this->Evento->obtenerTodos();
        $this->load->view('header');
        $this->load->view('eventos/listado', $data);
        $this->load->view('footer');
    }

    function nuevo()
    {
        $this->load->view('header');
        $this->load->view('eventos/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoEventos = array(
            "nombre_eve" => $this->input->post('nombre_eve'),
            "descripcion_eve" => $this->input->post('descripcion_eve'),
            "fecha_inicio_eve" => $this->input->post('fecha_inicio_eve'),
            "fecha_final_eve" => $this->input->post('fecha_final_eve'),
            "responsable_eve" => $this->input->post('responsable_eve'),
            "lugar_eve" => $this->input->post('lugar_eve'),
        );

        if($this->Evento->insertar($datosNuevoEventos)){
            redirect('eventos/listado');
        }else{
            echo "<h1> Error al insertar</h1>";
        }

    }

    public function eliminar($id_eve){
        if ($this->Evento->borrar($id_eve)) {
            redirect('eventos/listado');
            # code...
        } else {
            # code...
            echo "ERROR AL BORRAR :(";
        }
        
    }


    
}





?>